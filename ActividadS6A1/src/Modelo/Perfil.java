
package Modelo;

import java.util.ArrayList;
import java.util.List;


public class Perfil extends Estudiante{
    private int idPerfil;
    private static int contadorPerfil=0;
    private List<Grupo> grupos;
    private List<Publicacion> publicaciones;
    private List<Perfil> amigos;
    
    public Perfil(String nombre, String apellido, String nick, int edad, String contraseña, String correo) {
        super(nombre, apellido, nick, edad, contraseña, correo);
        this.grupos = new ArrayList<Grupo>();
        this.idPerfil = Perfil.contadorPerfil++;
    }
    
    public boolean validarPerfil(String nick,String contraseña){
        boolean b=false;        
        if(this.nick.equalsIgnoreCase(nick)&&this.contraseña.equalsIgnoreCase(contraseña))
            b=true;
        return b;
    }
    
    public void addAmigo(Perfil amigo){
        amigos.add(amigo);
    }
    public void addGrupo(Grupo grupo){
        grupos.add(grupo);
    }
    public void addPublicacion(Publicacion publicacion){
        publicaciones.add(publicacion);
    }

    public List<Perfil> getAmigos() {
        return amigos;
    }

    public void setAmigos(List<Perfil> amigos) {
        this.amigos = amigos;
    }
    
    public List<Publicacion> getPublicaciones() {
        return publicaciones;
    }

    public void setPublicaciones(List<Publicacion> publicaciones) {
        this.publicaciones = publicaciones;
    }
    
    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public static int getContadorPerfil() {
        return contadorPerfil;
    }

    public static void setContadorPerfil(int contadorPerfil) {
        Perfil.contadorPerfil = contadorPerfil;
    }

    public List<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<Grupo> grupos) {
        this.grupos = grupos;
    }
    
    
}
