
package Modelo;


public class Publicacion_Enl extends Publicacion {
    
    private String url;

    public Publicacion_Enl(String url, String mensaje) {
        super(mensaje);
        this.url = url;
    }    

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    

}
