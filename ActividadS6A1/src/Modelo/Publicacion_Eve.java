
package Modelo;


public class Publicacion_Eve extends Publicacion {

    private int ncupos;

    public Publicacion_Eve(int ncupos, String mensaje) {
        super(mensaje);
        this.ncupos = ncupos;
    }
    
    public int getNcupos() {
        return ncupos;
    }

    public void setNcupos(int ncupos) {
        this.ncupos = ncupos;
    }

}
