
package Modelo;


public class Grupo {
   private String nombreGrupo;
   private String descripcion;
   private String tipoGrupo;
   private String noticiasRecientes;
   private Perfil datosPropietario;

    public Grupo(String nombreGrupo, String descripcion, String tipoGrupo, String noticiasRecientes, Perfil datosPropietario) {
        this.nombreGrupo = nombreGrupo;
        this.descripcion = descripcion;
        this.tipoGrupo = tipoGrupo;
        this.noticiasRecientes = noticiasRecientes;
        this.datosPropietario = datosPropietario;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoGrupo() {
        return tipoGrupo;
    }

    public void setTipoGrupo(String tipoGrupo) {
        this.tipoGrupo = tipoGrupo;
    }

    public String getNoticiasRecientes() {
        return noticiasRecientes;
    }

    public void setNoticiasRecientes(String noticiasRecientes) {
        this.noticiasRecientes = noticiasRecientes;
    }

    public Perfil getDatosPropietario() {
        return datosPropietario;
    }

    public void setDatosPropietario(Perfil datosPropietario) {
        this.datosPropietario = datosPropietario;
    }
}
