
package Modelo;


public class Estudiante {

    protected String nombre;
    protected String apellido;
    protected String nick;
    protected int edad;
    protected String contraseña;
    protected String correo;
    protected int idEstudiante;
    protected static int contadorEstudiante;
    
    
    public Estudiante(String nombre, String apellido, String nick, int edad, String contraseña, String correo){
        this.nombre = nombre;
        this.apellido = apellido;
        this.nick = nick;
        this.edad = edad;
        this.contraseña = contraseña;
        this.correo = correo;
        this.idEstudiante = ++Estudiante.contadorEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getIdEstudiante() {
        return idEstudiante;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Estudiante{nombre=").append(nombre);
        sb.append(", apellido=").append(apellido);
        sb.append(", nick=").append(nick);
        sb.append(", edad=").append(edad);
        sb.append(", contrase\u00f1a=").append(contraseña);
        sb.append(", correo=").append(correo);
        sb.append(", idEstudiante=").append(idEstudiante);
        sb.append('}');
        return sb.toString();
    }
}
