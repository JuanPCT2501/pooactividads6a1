
package Modelo;


public class Comentario {
    private String comentario;
    private boolean reaccion;

    public Comentario(String comentario) {
        this.comentario = comentario;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public boolean isReaccion() {
        return reaccion;
    }

    public void setReaccion(boolean reaccion) {
        this.reaccion = reaccion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Comentario{comentario=").append(comentario);
        sb.append(", reaccion=").append(reaccion);
        sb.append('}');
        return sb.toString();
    }
    
    
}
