
package Modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Publicacion {
    private static final int  MAX_PUBLICACIONES = 10;
    private String fecha;
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    
    private String mensaje;

    public Publicacion(String mensaje) {        
        this.fecha = dtf.format(LocalDateTime.now());
        this.mensaje = mensaje;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    
    
    
    
    
}