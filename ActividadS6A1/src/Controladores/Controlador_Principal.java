
package Controladores;

import Modelo.Estudiante;
import Modelo.Grupo;
import Modelo.Perfil;
import Modelo.Publicacion;
import Modelo.Publicacion_Enl;
import Modelo.Publicacion_Eve;
import Modelo.Publicacion_Txt;
import Vistas.Vista_Grupo;
import Vistas.Vista_Perfil;
import Vistas.Vista_Principal;
import Vistas.Vista_PubliEnlace;
import Vistas.Vista_PubliEvento;
import Vistas.Vista_PubliTexto;
import Vistas.Vista_Publicacion;
import Vistas.Vista_Registro;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


public class Controlador_Principal implements ActionListener{
    
    private List<Perfil> perfiles;
    private Vista_Principal vistaprincipal=new Vista_Principal();
    private Vista_Perfil vistaperfil;
    private Vista_Registro vistaregistro;
    private Vista_Grupo vistagrupo;
    private Vista_Publicacion vistapublicacion;
    private Vista_PubliTexto vistapublitexto;
    private Vista_PubliEnlace vistapublienlace;
    private Vista_PubliEvento vistapublievento;
    private Perfil perfilactual;
    
    public Controlador_Principal() {
        this.perfiles = new ArrayList<Perfil>();
        this.vistaprincipal.setVisible(true);
        actionListener(this);
    }
    
    
    private void actionListener(ActionListener controlador){
        vistaprincipal.addbotonloginActionListener(controlador);
        vistaprincipal.addbotonregistrarActionListener(controlador);
        vistaregistro.addbotonregistroActionListener(controlador);
        vistaperfil.addbotonbuscarActionListener(controlador);
        vistaperfil.addbotonnuevogrupoActionListener(controlador);
        vistaperfil.addbotonnuevapublicacionActionListener(controlador);
    }
    
    @Override
    public void actionPerformed(ActionEvent evento) {
        try {
            if(evento.getActionCommand().contentEquals("Login")){
                String nick,contraseña;
                nick=vistaprincipal.getTxtnombre();
                contraseña=vistaprincipal.getTxtcontraseña();
                
                for(Perfil perfil: perfiles){
                    if(perfil.validarPerfil(nick, contraseña)){ 
                        perfilactual=perfil;
                        vistaperfil=new Vista_Perfil(perfilactual);
                        actionListener(this);
                    }
                }
            }else{
            if(evento.getActionCommand().contentEquals("Registrarse")){
                vistaprincipal.setVisible(false);
                vistaprincipal.clear();
                vistaregistro=new Vista_Registro();
                actionListener(this);
            }else{
            if(evento.getActionCommand().contentEquals("Registrarme")){
                perfiles.add(new Perfil(vistaregistro.getTxtnombre(),vistaregistro.getTxtapellido(),vistaregistro.getTxtnick(),vistaregistro.getTxtedad(),
                vistaregistro.getTxtcontraseña(),vistaregistro.getTxtcorreo()));                    
                vistaregistro.dispose();
                vistaprincipal.setVisible(true);
            }else{
            if(evento.getActionCommand().contentEquals("Nuevo Grupo")){
                vistaperfil.setVisible(false);
                vistagrupo = new Vista_Grupo();
                actionListener(this);
            }else{
            if(evento.getActionCommand().contentEquals("Crear Grupo")){
                perfilactual.getGrupos().add(new Grupo(vistagrupo.getTxtnombregrupo(), vistagrupo.getTxtdescripcion(), 
                        vistagrupo.getTxttipogrupo(),vistagrupo.getTxtnoticias(), perfilactual));
                vistagrupo.dispose();
                vistaperfil.setVisible(true);
            }
            else{
            if(evento.getActionCommand().contentEquals("Nueva Publicacion")){
                vistapublicacion=new Vista_Publicacion();
                actionListener(this);
            }
            else{
            if(evento.getActionCommand().contentEquals("Publicacion textual")){
                vistapublicacion.dispose();
                vistapublitexto=new Vista_PubliTexto();
                actionListener(this);
            }
            else{
            if(evento.getActionCommand().contentEquals("Publicacion de enlace")){
                vistapublicacion.dispose();
                vistapublienlace=new Vista_PubliEnlace();
                actionListener(this);
            }
            else{
            if(evento.getActionCommand().contentEquals("Publicacion de evento")){
                vistapublicacion.dispose();
                vistapublitexto=new Vista_PubliTexto();
                actionListener(this);
            }
            else{
            if(evento.getActionCommand().contentEquals("Crear Publicacion Textual")){
            perfilactual.addPublicacion(new Publicacion_Txt(vistapublitexto.getTxtMensaje()));
            vistapublitexto.dispose();
            }
            else{
            if(evento.getActionCommand().contentEquals("Crear Publicacion de Enlace")){
            perfilactual.addPublicacion(new Publicacion_Enl(vistapublienlace.getTxtUrl(),vistapublienlace.getTxtMensaje()));
            vistapublienlace.dispose();
            }
            else{
            if(evento.getActionCommand().contentEquals("Crear Publicacion de Evento")){
            perfilactual.addPublicacion(new Publicacion_Eve(vistapublievento.getCupos(),vistapublievento.getTxtMensaje()));
            vistapublievento.dispose();
            }
            }
            }
            
            }
            }
            }
            }
            }                
            }               
            }
            }
            }            
        }
        catch(Exception e){
            e.printStackTrace();
        }
            
    }
    public static void main( String[] args )
    {
        Controlador_Principal controlador=new Controlador_Principal();
    }
}
